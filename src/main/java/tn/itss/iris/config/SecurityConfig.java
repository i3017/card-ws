package tn.itss.iris.config;

import static org.springframework.http.HttpMethod.DELETE;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.HttpMethod.PUT;
import static tn.itss.iris.enums.Role.ADMIN;
import static tn.itss.iris.enums.Role.USER;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import lombok.RequiredArgsConstructor;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfig {

    @Value("${project.version}")
    private String version;

    private final JwtAuthenticationFilter jwtAuthFilter;
    private final AuthenticationProvider authenticationProvider;

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http
                .csrf()
                .disable()
                .authorizeHttpRequests()
                .antMatchers("/api/v" + version + "/auth/**")
                .permitAll()

                .antMatchers("/api/v" + version + "/customers/**", "/api/v" + version + "/users/**").hasAnyRole("ADMIN")
        
        
                .antMatchers(POST, "/api/v" + version + "/carts").hasAnyAuthority(USER.name(), ADMIN.name())
                .antMatchers(PUT, "/api/v" + version + "/carts").hasAnyAuthority(USER.name(), ADMIN.name())
                .antMatchers(DELETE, "/api/v" + version + "/carts").hasAnyAuthority(USER.name(), ADMIN.name())
                .antMatchers(GET, "/api/v" + version + "/carts").hasAnyAuthority(USER.name(), ADMIN.name())


                .antMatchers(POST, "/api/v" + version + "/transfers").hasAnyAuthority(USER.name(), ADMIN.name())
                .antMatchers(PUT, "/api/v" + version + "/transfers").hasAnyAuthority(USER.name(), ADMIN.name())
                .antMatchers(DELETE, "/api/v" + version + "/transfers").hasAnyAuthority(USER.name(), ADMIN.name())
                .antMatchers(GET, "/api/v" + version + "/transfers").hasAnyAuthority(USER.name(), ADMIN.name())

                .anyRequest()
                .authenticated()
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authenticationProvider(authenticationProvider)
                .addFilterBefore(jwtAuthFilter, UsernamePasswordAuthenticationFilter.class);

        return http.build();
    }
}
