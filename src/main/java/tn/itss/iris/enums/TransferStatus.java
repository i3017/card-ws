package tn.itss.iris.enums;

public enum TransferStatus {
    PENDING,
    SUCCESS,
    FAILED,
    CANCELLED,
    REFUNDED,
    REJECTED
}
