package tn.itss.iris.enums;

public enum CardStatus {
    ACITVE,
    BLOCKED,
    DELETED,
    EXPIRED,
    PENDING, DISABLE
}
