package tn.itss.iris.enums;

public enum CardType {
    VISA("Visa", "453201", "JPMorgan Chase Bank", 16),
    MASTERCARD("Mastercard", "510029", "Capital One Bank", 16),
    AMERICAN_EXPRESS("American Express", "371449", "American Express Company", 15),
    DISCOVER("Discover", "601111", "Discover Financial Services", 16),
    DINERS_CLUB("Diners Club", "300011", "BMO Harris Bank", 14);

    private final String cardName;
    private final String bin;
    private final String issuer;
    private final int cardLength;

    CardType(String cardName, String bin, String issuer, int cardLength) {
        this.cardName = cardName;
        this.bin = bin;
        this.issuer = issuer;
        this.cardLength = cardLength;
    }

    public String getCardName() {
        return cardName;
    }

    public String getBin() {
        return bin;
    }

    public String getIssuer() {
        return issuer;
    }

    public int getCardLength() {
        return cardLength;
    }

    public static CardType getCardTypeByName(String name) {
        for (CardType type : CardType.values()) {
            if (type.getCardName().toUpperCase().equals(name.toUpperCase())) {
                return type;
            }
        }
        return null;
    }

}