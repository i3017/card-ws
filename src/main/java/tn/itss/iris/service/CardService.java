package tn.itss.iris.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import tn.itss.iris.enums.CardStatus;
import tn.itss.iris.model.Card;
import tn.itss.iris.repository.CardRepository;

@Service
@RequiredArgsConstructor
@Slf4j
public class CardService {

    private final CardRepository repository;
    private final SequenceGenerator sequenceGenerator;

    public List<Card> getAllCards() {
        return repository.findAll();
    }

    public List<Card> getAllActiveCards() {
        return repository.findAllByCardStatus(CardStatus.ACITVE);
    }

    public List<Card> getAllCardsByClient(String cin) {
        return repository.findAllByCin(cin);
    }

    public Long addCard(Card u) {
        log.info("Adding card : " + u);
        u.setId(sequenceGenerator.generateSequence(Card.SEQUENCE_NAME));

        return repository.save(u).getId();
    }

    public Boolean deleteCard(String cardNumber) {
        Card c = repository.findByCardNumber(cardNumber).orElse(null);
        log.info("Deleting card with cardNumber : " + cardNumber);
        if (c != null) {

            repository.deleteById(c.getId());

            return true;
        } else
            return false;
    }

    public Card updateCard(Card c) {
        log.info("Updating customer : " + c);
        if (getCard(c.getId()) != null)
            return repository.save(c);
        else
            return null;
    }

    public Card getCard(Long id) {
        log.info("in  getCard id = " + id);
        Card c = repository.findById(id).orElse(null);
        log.info("customer returned : " + c);
        return c;
    }

    public Long addCardByClient(Card card) {
        log.info(card.toString());
        Card c = repository.findByCardNumber(card.getCardNumber())
                .orElse(null);

        if (c == null)
            return null;
        else {
            log.info(c.toString());
            if (c.getCardType().toUpperCase().equals(card.getCardType().toUpperCase())
                    && c.getExpiryDate().equals(card.getExpiryDate())
                    && c.getCvv() == card.getCvv()) {
                c.setCardStatus(CardStatus.ACITVE);
                c.setCardHolderName(card.getCardHolderName());
                return repository.save(c).getId();
            } else
                return null;
        }

    }

    public Card getCardByCardNumber(String cardNumber) {
        Card c = repository.findByCardNumber(cardNumber).orElse(null);
        return c;
    }

    public boolean disableCard(@Valid String cardNumber) {
        Card c = repository.findByCardNumber(cardNumber).orElse(null);
        log.info("Deleting card with cardNumber : " + cardNumber);
        if (c != null) {
            c.setCardStatus(CardStatus.DISABLE);
            repository.save(c);

            return true;
        } else
            return false;
    }

}
