package tn.itss.iris.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import tn.itss.iris.enums.TransferStatus;
import tn.itss.iris.model.Card;
import tn.itss.iris.model.Transfer;
import tn.itss.iris.repository.CardRepository;
import tn.itss.iris.repository.TransferRepository;

@Service
@RequiredArgsConstructor
@Slf4j
public class TransferService {

    private final TransferRepository repository;
    private final SequenceGenerator sequenceGenerator;
    private final CardService cardService;
    private final CardRepository cardRepository;

    public Long makeTransfer(Transfer t) {
        log.info("make a transfer : " + t);
        t.setId(sequenceGenerator.generateSequence(Transfer.SEQUENCE_NAME));
        t.setTimeStamp(LocalDateTime.now());
        List<String> errors = new ArrayList<>();
        Card debitCard = cardService.getCardByCardNumber(t.getDebitAccount());
        Card creditCard = cardService.getCardByCardNumber(t.getCreditAccount());
        if (debitCard == null)
            errors.add("Debit Account not found");
        if (creditCard == null)
            errors.add("Credit Account not found");
        if (debitCard != null && creditCard != null) {
            if (debitCard.getCardBalance() >= t.getAmount()) {
                t.setStatus(TransferStatus.SUCCESS);
                debitCard.setCardBalance(debitCard.getCardBalance() - t.getAmount());
                creditCard.setCardBalance(debitCard.getCardBalance() + t.getAmount());
                cardRepository.save(debitCard);
                cardRepository.save(creditCard);
                t.setError(null);
                t.setStatus(TransferStatus.SUCCESS);
            } else {
                errors.add("Insufficient balance");
                t.setStatus(TransferStatus.FAILED);
            }
        }
        t.setError(errors.toString());
        if (!errors.isEmpty())
            t.setStatus(TransferStatus.FAILED);
            
        return repository.save(t).getId();
    }

    public List<Transfer> getAllTransfers() {
        return repository.findAll();
    }

    public List<Transfer> getAllTransfersByClient(String cin) {
        List<Transfer> result = repository.findAllByCreditAccount(cin);
        result.addAll(repository.findAllByDebitAccount(cin));
        return result;
    }

    public Transfer getTransfer(Long id) {
        log.info("in  getTransfer with id = " + id);
        Transfer t = repository.findById(id).orElse(null);
        log.info("Transfer returned : " + t);
        return t;
    }
}
