package tn.itss.iris.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import tn.itss.iris.model.Transfer;

@Repository
public interface TransferRepository extends MongoRepository<Transfer, Long>{

    List<Transfer> findAllByCreditAccount(String cin);
    List<Transfer> findAllByDebitAccount(String cin);
    
}
