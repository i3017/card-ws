package tn.itss.iris.repository;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import tn.itss.iris.model.User;


@Repository
public interface UserRepository extends MongoRepository<User, Long>{
    
    User findByEmailAndPassword(String email, String password);

    Optional<User> findByEmail(String email);
}
