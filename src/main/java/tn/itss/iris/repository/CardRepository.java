package tn.itss.iris.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import tn.itss.iris.enums.CardStatus;
import tn.itss.iris.model.Card;

@Repository
public interface CardRepository extends MongoRepository<Card, Long>{

    Optional<Card> findByCardNumber(String cardNumber);

    List<Card> findAllByCardStatus(CardStatus acitve);

    List<Card> findAllByCin(String cin);
    
}
