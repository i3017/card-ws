package tn.itss.iris.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import tn.itss.iris.dto.TransferRequest;
import tn.itss.iris.enums.TransferStatus;
import tn.itss.iris.model.Transfer;
import tn.itss.iris.service.TransferService;

@RestController
@RequestMapping("/transfers")
@Slf4j
@RequiredArgsConstructor
public class TransferController {

    private final TransferService service;

    @PostMapping
    public ResponseEntity<TransferRequest> createTransfer(@Valid @RequestBody TransferRequest request) {
        log.info(request.toString());
        Transfer transfer = Transfer
                .builder()
                .debitAccount(request.getDebitAccount())
                .creditAccount(request.getCreditAccount())
                .motif(request.getMotif())
                .amount(request.getAmount())
                .currency(request.getCurrency())
                .timeStamp(LocalDateTime.now())
                .status(TransferStatus.PENDING)
                .build();

        Long result = service.makeTransfer(transfer);
        Transfer transferAdded = service.getTransfer(result);

        TransferRequest transferResponse = TransferRequest
                .builder()
                .debitAccount(transferAdded.getDebitAccount())
                .creditAccount(transferAdded.getCreditAccount())
                .motif(transferAdded.getMotif())
                .amount(transferAdded.getAmount())
                .currency(transferAdded.getCurrency())
                .timeStamp(transferAdded.getTimeStamp())
                .error(transferAdded.getError())
                .status(transferAdded.getStatus().name())
                .build();

        return ResponseEntity.ok(transferResponse);

    }

    @GetMapping
    public ResponseEntity<TransferRequest> retreiveTransferById(@PathVariable("id") @Valid Long id) {

        Transfer transfer = service.getTransfer(id);

        TransferRequest transferResponse = TransferRequest
                .builder()
                .debitAccount(transfer.getDebitAccount())
                .creditAccount(transfer.getCreditAccount())
                .motif(transfer.getMotif())
                .amount(transfer.getAmount())
                .currency(transfer.getCurrency())
                .timeStamp(transfer.getTimeStamp())
                .error(transfer.getError())
                .status(transfer.getStatus().name())
                .build();

        return ResponseEntity.ok(transferResponse);

    }

    @GetMapping("/cin/{cin}")
    public ResponseEntity<List<TransferRequest>> retreiveAllTransfersByClient(@PathVariable("cin") @Valid String cin) {

        List<Transfer> transfers = service.getAllTransfersByClient(cin);
        List<TransferRequest> cardResponses = new ArrayList<TransferRequest>();

        for (Transfer transfer : transfers) {

            TransferRequest transferResponse = TransferRequest
                .builder()
                .debitAccount(transfer.getDebitAccount())
                .creditAccount(transfer.getCreditAccount())
                .motif(transfer.getMotif())
                .amount(transfer.getAmount())
                .currency(transfer.getCurrency())
                .timeStamp(transfer.getTimeStamp())
                .error(transfer.getError())
                .status(transfer.getStatus().name())
                .build();

            cardResponses.add(transferResponse);
        }

        return ResponseEntity.ok(cardResponses);

    }
}
