package tn.itss.iris.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import tn.itss.iris.dto.CardRequestBack;
import tn.itss.iris.dto.CardRequestFront;
import tn.itss.iris.dto.CardResponse;
import tn.itss.iris.enums.CardStatus;
import tn.itss.iris.enums.CardType;
import tn.itss.iris.model.Card;
import tn.itss.iris.service.CardService;
import tn.itss.iris.service.CreditCardNumberGenerator;

@RestController
@RequestMapping("/cards")
@Slf4j
@RequiredArgsConstructor
public class CardController {

    private final CardService service;
    private final CreditCardNumberGenerator creditCardNumberGenerator;

    @PostMapping
    public ResponseEntity<CardResponse> createCard(@Valid @RequestBody CardRequestBack request) {
        log.info(request.toString());
        CardType cardType = CardType.getCardTypeByName(request.getCardType());
        Card card = Card
                .builder()
                .cardNumber(creditCardNumberGenerator.generate(cardType.getBin(), cardType.getCardLength()))
                .cin(request.getCin())
                .cardType(request.getCardType())
                .cardCurrency(request.getCardCurrency())
                .cardBalance(request.getCardBalance())
                .cardStatus(CardStatus.PENDING)
                .expiryDate(request.getExpiryDate())
                .cvv(request.getCvv())
                .build();

        Long result = service.addCard(card);
        Card cardAdded = service.getCard(result);

        CardResponse cardResponse = CardResponse
                .builder()
                .cardNumber(cardAdded.getCardNumber())
                .cardHolderName(cardAdded.getCardHolderName())
                .cardType(cardAdded.getCardType())
                .cardStatus(cardAdded.getCardStatus().name())
                .cardCurrency(cardAdded.getCardCurrency())
                .cardBalance(cardAdded.getCardBalance())
                .expiryDate(cardAdded.getExpiryDate())
                .cvv(cardAdded.getCvv())
                .build();

        return ResponseEntity.ok(cardResponse);

    }

    @PutMapping
    public ResponseEntity<CardResponse> updateCard(@Valid @RequestBody CardRequestBack request) {
        log.info(request.toString());
        Card card = Card
                .builder()
                .cin(request.getCin())
                .cardType(request.getCardType())
                .cardCurrency(request.getCardCurrency())
                .cardBalance(request.getCardBalance())
                .cardStatus(CardStatus.PENDING)
                .build();

        Card cardUpdated = service.updateCard(card);

        CardResponse cardResponse = CardResponse
                .builder()
                .cardNumber(cardUpdated.getCardNumber())
                .cardHolderName(cardUpdated.getCardHolderName())
                .cardType(cardUpdated.getCardType())
                .cardStatus(cardUpdated.getCardStatus().name())
                .cardCurrency(cardUpdated.getCardCurrency())
                .cardBalance(cardUpdated.getCardBalance())
                .expiryDate(cardUpdated.getExpiryDate())
                .cvv(cardUpdated.getCvv())
                .build();

        return ResponseEntity.ok(cardResponse);

    }

    @PostMapping("/activate")
    public ResponseEntity<CardResponse> addCardByClient(@Valid @RequestBody CardRequestFront request) {
        log.info(request.toString());
        Card card = Card
                .builder()
                .cardNumber(request.getCardNumber())
                .cardType(request.getCardType())
                .cardHolderName(request.getCardHolderName())
                .expiryDate(request.getExpiryDate())
                .cvv(request.getCvv())
                .build();

        Long result = service.addCardByClient(card);
        Card cardAdded = service.getCard(result);

        CardResponse cardResponse = CardResponse
                .builder()
                .cardNumber(cardAdded.getCardNumber())
                .cardHolderName(cardAdded.getCardHolderName())
                .cardType(cardAdded.getCardType())
                .cardStatus(cardAdded.getCardStatus().name())
                .cardCurrency(cardAdded.getCardCurrency())
                .cardBalance(cardAdded.getCardBalance())
                .expiryDate(cardAdded.getExpiryDate())
                .cvv(cardAdded.getCvv())
                .build();

        return ResponseEntity.ok(cardResponse);

    }

    @DeleteMapping
    public ResponseEntity<CardResponse> deleteCard(@Valid @RequestBody String cardNumber) {
        log.info(cardNumber);

        boolean result = service.deleteCard(cardNumber);
        Card cardAdded = service.getCardByCardNumber(cardNumber);

        CardResponse cardResponse = CardResponse
                .builder()
                .cardNumber(cardAdded.getCardNumber())
                .cardHolderName(cardAdded.getCardHolderName())
                .cardType(cardAdded.getCardType())
                .cardStatus(cardAdded.getCardStatus().name())
                .cardCurrency(cardAdded.getCardCurrency())
                .cardBalance(cardAdded.getCardBalance())
                .expiryDate(cardAdded.getExpiryDate())
                .cvv(cardAdded.getCvv())
                .build();

        if (result)
            return ResponseEntity.ok(cardResponse);
        else
            return ResponseEntity.badRequest().body(cardResponse);

    }

    @DeleteMapping("/disable")
    public ResponseEntity<CardResponse> disableCard(@Valid @RequestBody String cardNumber) {
        log.info(cardNumber);

        boolean result = service.disableCard(cardNumber);
        Card cardAdded = service.getCardByCardNumber(cardNumber);

        CardResponse cardResponse = CardResponse
                .builder()
                .cardNumber(cardAdded.getCardNumber())
                .cardHolderName(cardAdded.getCardHolderName())
                .cardType(cardAdded.getCardType())
                .cardStatus(cardAdded.getCardStatus().name())
                .cardCurrency(cardAdded.getCardCurrency())
                .cardBalance(cardAdded.getCardBalance())
                .expiryDate(cardAdded.getExpiryDate())
                .cvv(cardAdded.getCvv())
                .build();

        if (result)
            return ResponseEntity.ok(cardResponse);
        else
            return ResponseEntity.badRequest().body(cardResponse);

    }

    @GetMapping
    public ResponseEntity<List<Card>> retreiveAllCards() {

        List<Card> cards = service.getAllCards();

        return ResponseEntity.ok(cards);

    }

    @GetMapping("/cin/{cin}")
    public ResponseEntity<List<CardResponse>> retreiveAllCardsByClient(@PathVariable("cin") @Valid String cin) {

        List<Card> cards = service.getAllCardsByClient(cin);
        List<CardResponse> cardResponses = new ArrayList<CardResponse>();

        for (Card card : cards) {

            CardResponse cardResponse = CardResponse
                    .builder()
                    .cardNumber(card.getCardNumber())
                    .cardHolderName(card.getCardHolderName())
                    .cardType(card.getCardType())
                    .cardStatus(card.getCardStatus().name())
                    .cardCurrency(card.getCardCurrency())
                    .cardBalance(card.getCardBalance())
                    .expiryDate(card.getExpiryDate())
                    .cvv(card.getCvv())
                    .build();

            cardResponses.add(cardResponse);
        }

        return ResponseEntity.ok(cardResponses);

    }

    @GetMapping("/active")
    public ResponseEntity<List<CardResponse>> retreiveAllActiveCards() {

        List<Card> cards = service.getAllActiveCards();
        List<CardResponse> cardResponses = new ArrayList<CardResponse>();

        for (Card card : cards) {

            CardResponse cardResponse = CardResponse
                    .builder()
                    .cardNumber(card.getCardNumber())
                    .cardHolderName(card.getCardHolderName())
                    .cardType(card.getCardType())
                    .cardStatus(card.getCardStatus().name())
                    .cardCurrency(card.getCardCurrency())
                    .cardBalance(card.getCardBalance())
                    .expiryDate(card.getExpiryDate())
                    .cvv(card.getCvv())
                    .build();

            cardResponses.add(cardResponse);
        }

        return ResponseEntity.ok(cardResponses);

    }

    @GetMapping("/{id}")
    public ResponseEntity<CardResponse> retreiveCardById(@PathVariable("id") @Valid Long id) {

        Card card = service.getCard(id);

        CardResponse cardResponse = CardResponse
                .builder()
                .cardNumber(card.getCardNumber())
                .cardHolderName(card.getCardHolderName())
                .cardType(card.getCardType())
                .cardStatus(card.getCardStatus().name())
                .cardCurrency(card.getCardCurrency())
                .cardBalance(card.getCardBalance())
                .expiryDate(card.getExpiryDate())
                .cvv(card.getCvv())
                .build();

        return ResponseEntity.ok(cardResponse);

    }
}
