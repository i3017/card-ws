package tn.itss.iris.dto;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import tn.itss.iris.enums.CardType;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CardRequest {

    private String cardNumber;
    private String cardHolderName;
    private CardType cardType;
    private String cardStatus;
    private String cardCurrency;
    private float cardBalance;
    private Date expiryDate;
    private int cvv;
}
