package tn.itss.iris.dto;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TransferRequest {
    
    private Long id;
    private String debitAccount;
    private String creditAccount;
    private String motif;
    private float amount;
    private String currency;
    private LocalDateTime timeStamp;
    private String error;
    private String status;
}
