package tn.itss.iris.dto;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CardRequestFront {
    
    private String cardNumber;
    private String cardHolderName;
    private String cardType;
    private Date expiryDate;
    private int cvv;
}
