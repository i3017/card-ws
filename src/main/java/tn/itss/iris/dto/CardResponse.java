package tn.itss.iris.dto;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CardResponse {

    private String cardNumber;
    private String cardHolderName;
    private String cardType;
    private String cardStatus;
    private String cardCurrency;
    private float cardBalance;
    private Date expiryDate;
    private int cvv;
    
}
