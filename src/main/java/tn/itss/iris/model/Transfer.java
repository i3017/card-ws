package tn.itss.iris.model;

import java.time.LocalDateTime;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import tn.itss.iris.enums.TransferStatus;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Document(collection = "transfers")
public class Transfer {

	@Transient
	public static final String SEQUENCE_NAME = "transfers_sequence";
    
    @Id
    private Long id;

    private String debitAccount;
    private String creditAccount;
    private String motif;
    private float amount;
    private String currency;
    private LocalDateTime timeStamp;
    private String error;
    private TransferStatus status;

}
