package tn.itss.iris.model;

import java.util.Date;

import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import nonapi.io.github.classgraph.json.Id;
import tn.itss.iris.enums.CardStatus;
import tn.itss.iris.enums.CardType;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "cards")
@Builder
public class Card {

	@Transient
	public static final String SEQUENCE_NAME = "carts_sequence";
    
    @Id
    private Long id;

    private String cin;
    private String cardNumber;
    private String cardHolderName;
    private String cardType;
    private CardStatus cardStatus;
    private String cardCurrency;
    private float cardBalance;
    private Date expiryDate;
    private int cvv;

}
