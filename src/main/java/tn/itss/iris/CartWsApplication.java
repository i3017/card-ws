package tn.itss.iris;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CartWsApplication {

	public static void main(String[] args) {
		SpringApplication.run(CartWsApplication.class, args);
	}

}
