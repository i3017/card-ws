# Cart WS

## Bank Identification Numbers (BINs) for different card networks:

1. Visa:
   - BIN: 453201
   - Issuing Bank: JPMorgan Chase Bank

2. Mastercard:
   - BIN: 510029
   - Issuing Bank: Capital One Bank

3. American Express (Amex):
   - BIN: 371449
   - Issuing Bank: American Express Company

4. Discover:
   - BIN: 601111
   - Issuing Bank: Discover Financial Services

5. Diners Club:
   - BIN: 300011
   - Issuing Bank: BMO Harris Bank

Please note that these are just example BINs, and the actual BINs used by card networks may vary. Each BIN corresponds to a specific issuing bank or financial institution, and the range of BINs assigned to each network is extensive.

You can find more comprehensive BIN lists and lookup services online, which provide detailed information about BIN ranges and their corresponding banks or card issuers.